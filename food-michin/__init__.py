class Mascota(object):

    def __init__(self, nombre):
        self.nombre = nombre
        self.comida = 6

    def hambre(self):
        self.comida = self.comida - 1
        return self.comida

    def sueño(self):
        self.comida = self.comida - 0.2
        return self.comida 
