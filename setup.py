import setuptools

with open("README.md", "r") as fh:
  long_description = fh.read()

setuptools.setup(
  name = 'foodmichin',
  packages = setuptools.find_packages(), # this must be the same as the name above
  version = '0.2',
  description = 'food for michin',
  author = 'Udalber Rodriguez',
  author_email = 'udgottschalk@gmail.com',
  url = 'https://gitlab.com/Udalbert/food-michin', # use the URL to the github repo
  download_url = 'https://gitlab.com/Udalbert/food-michin/tags/0.2',
  keywords = ['food-neko', 'food-michin', 'example'],
  classifiers = [],
)